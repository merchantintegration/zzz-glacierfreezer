import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.filefilter.WildcardFileFilter;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.amazonaws.services.glacier.model.GetJobOutputRequest;
import com.amazonaws.services.glacier.model.GetJobOutputResult;
import com.amazonaws.services.glacier.model.GlacierJobDescription;
import com.amazonaws.services.glacier.model.InitiateJobRequest;
import com.amazonaws.services.glacier.model.JobParameters;
import com.amazonaws.services.glacier.model.ListJobsRequest;
import com.amazonaws.services.glacier.model.ListJobsResult;
import com.amazonaws.services.glacier.transfer.ArchiveTransferManager;
import com.amazonaws.services.glacier.transfer.UploadResult;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.BatchPutAttributesRequest;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.ReplaceableItem;

// amazon endpoints: http://docs.amazonwebservices.com/general/latest/gr/rande.html#glacier_region
// 1	US East (Northern Virginia) Region		glacier.us-east-1.amazonaws.com
// 2	US West (Oregon) Region					glacier.us-west-2.amazonaws.com
// 3	US West (Northern California) Region	glacier.us-west-1.amazonaws.com
// 4	EU (Ireland) Region						glacier.eu-west-1.amazonaws.com
// 5	Asia Pacific (Tokyo) Region				glacier.ap-northeast-1.amazonaws.com

public class glacierFreezer {

	static Properties conf = new Properties();
	
	static String vaultName = null;
	static String accessKey = "";
	static String secretKey = "";
	static String simpleDbDomainName = "";
	static String endPoint = "";
	static Integer endPointInt = 0;
	static String propertiesFile = System.getProperty("user.home") + "/.glacierFreezer.properties";

	static String[] endPointList = {
		"https://glacier.us-east-1.amazonaws.com",
		"https://glacier.us-west-2.amazonaws.com",
		"https://glacier.us-west-1.amazonaws.com",
		"https://glacier.eu-west-1.amazonaws.com",
		"https://glacier.ap-northeast-1.amazonaws.com",
	};
	
	static AWSCredentials credentials;
	static AmazonSimpleDB sdb;
	static AmazonGlacierClient glacierClient;
	
	static ListJobsRequest listInProgressJobsReq = null;
	static ListJobsRequest listJobsReq = null;
	static ListJobsResult jobsList = new ListJobsResult();
	
	// http://docs.amazonwebservices.com/AWSJavaSDK/latest/javadoc/index.html?com/amazonaws/services/glacier/transfer/ArchiveTransferManager.html
	private static void freezeFile(String fileName) {
		String newArchiveId = null;
		
		String filePathOnly = new File(fileName).getParent();
		String fileNameOnly = new File(fileName).getName();
		
		System.out.println("Processing file " + fileName);
		try {
			ArchiveTransferManager atm = new ArchiveTransferManager(glacierClient, credentials);
			String comment = fileNameOnly + "|" + fileName;
			
			UploadResult result = atm.upload(vaultName, comment, new File(filePathOnly, fileNameOnly));
			
			newArchiveId = result.getArchiveId();
			System.out.println("File " +  fileName + " uploaded as " + fileNameOnly + ". Archive ID: " + newArchiveId);
			
		} catch (Exception e) {
			System.out.println("Error uploading file " + fileName);
			System.err.println(e);
			System.exit(1);
		}
		
		if (newArchiveId == null) {
			System.out.println("Error uploading file (could not get ArchiveId) " + fileName);
			System.exit(1);
		}
		
		// create a record in simpledb
		// make sure domain exists
		if (simpleDbDomainName.length() > 0) {
			try {
				sdb.createDomain(new CreateDomainRequest(simpleDbDomainName));
			} catch (AmazonServiceException ase) {
				System.out.println("File upload successful, however there was an error while creating simpleDb domain. " + ase);
				System.exit(1);
			}
			
			long fileSize = new File(fileName).length();
			
			try {
				ReplaceableItem item = new ReplaceableItem(newArchiveId).withAttributes(
					new ReplaceableAttribute("vaultName", vaultName, true),
					new ReplaceableAttribute("fileName", fileNameOnly, true),
					new ReplaceableAttribute("archiveId", newArchiveId, true),
					new ReplaceableAttribute("localDateTime", (new Date().toString()), true),
					new ReplaceableAttribute("sizeBytes", String.valueOf(fileSize), true)
				);
				sdb.batchPutAttributes(new BatchPutAttributesRequest().withDomainName(simpleDbDomainName).withItems(item));
				
			} catch (AmazonServiceException ase) {
				System.out.println("Caught Exception: " + ase.getMessage());
				System.out.println("Reponse Status Code: " + ase.getStatusCode());
				System.out.println("Error Code: " + ase.getErrorCode());
				System.out.println("Request ID: " + ase.getRequestId());
				System.out.println("File upload successful, however there was an error while creating simpleDb record (see above for details).");
				
			}
		}
	}
	
	/*
	private static void reconcileVault() {
		
		// get a list of current Jobs for this vault
		// http://docs.amazonwebservices.com/sdkfornet/latest/apidocs/html/M_Amazon_Glacier_AmazonGlacier_ListJobs.htm
		if (listJobsReq == null) {
			listJobsReq = new ListJobsRequest().withVaultName(vaultName);
			try {
				jobsList = glacierClient.listJobs(listJobsReq);
			} catch (Exception e) {
				System.out.println("Can not obtain list jobs for vault " + vaultName + ": " + e);
				System.exit(1);
			}
		}
		
		// iterate through the job list and check if there is a InventoryRetrieval job?
		String inventoryRetrievalJobId = null;
		String inventoryRetrievalJobStatus = null;
		GlacierJobDescription jobDescription = null;
		List<GlacierJobDescription> listOfJobs = jobsList.getJobList();
		for (int i = 0; i < listOfJobs.size(); i++) {
			jobDescription = listOfJobs.get(i);
			if (jobDescription.getAction().equals("InventoryRetrieval")) {
				inventoryRetrievalJobStatus = jobDescription.getStatusCode(); // InProgress Succeeded
				if ((inventoryRetrievalJobStatus.equals("InProgresss")) || (inventoryRetrievalJobStatus.equals("Succeeded"))) {
					inventoryRetrievalJobId = jobDescription.getJobId();
					break;
				}
			}
		}
		
		if (inventoryRetrievalJobId == null) {
			// get detailed info -- request
			// http://docs.amazonwebservices.com/amazonglacier/latest/dev/retrieving-vault-inventory-java.html
			JobParameters jobParam = new JobParameters();
			jobParam.setType("inventory-retrieval");
			
			InitiateJobRequest jobReq = new InitiateJobRequest().withVaultName(vaultName).withJobParameters(jobParam);
			String initiatedJobRequestId = glacierClient.initiateJob(jobReq).toString();
			
			System.out.println("Inventory retrieval request has been submitted for vault " + vaultName + ". It can take up to 24 hours to complete, and once it does you need to re-run glacierFreezer so it can download the data."); // Job ID is: " + initiatedJobRequestId);
		} else {
			
			// job exists. is it in progress or completed?
			if (inventoryRetrievalJobStatus.equals("InProgress")) {
				System.out.println("Inventory retrieval is currently in progress and being processed by Amazon's servers. It can take up to 24 hours to complete, and once it does you need to re-run glacierFreezer so it can download the data.");
				
			} else {
				String jsonResult = "";
				
				// job has Succeeded. get results
				try {
					GetJobOutputRequest jobOutputRequest = new GetJobOutputRequest()
						.withVaultName(vaultName)
						.withJobId(inventoryRetrievalJobId);
					GetJobOutputResult jobOutputResult = glacierClient.getJobOutput(jobOutputRequest);
					
					BufferedReader in = new BufferedReader(new InputStreamReader(jobOutputResult.getBody()));
					String inputLine;
					try {
						while ((inputLine = in.readLine()) != null) {
							jsonResult = jsonResult + inputLine;
						}
					} catch (IOException e) {
						jsonResult = null;
					}
					
					System.out.println(jsonResult);
					
				} catch (AmazonServiceException e) {
					System.out.println("There was an error retrieving the result from Amazon");
					System.exit(1);
					
				}
				
				// Example result {"VaultARN":"arn:aws:glacier:us-east-1:462549522606:vaults/gogo-pictures","InventoryDate":"2012-08-23T17:55:39Z","ArchiveList":[{"ArchiveId":"phK8AgtL7PzsKWSe7N11tHiUUgFAZrvRw7kqnnBcZW1SFHCObUuI6mO_moCm1RM9MAByOlMm_o7jREJxxABoTBqc9M1__5vqr2dM4MLSBiAjGwMGS3sBLK5X6D6fX9iqTC2yBMS6jQ","ArchiveDescription":"my archive Tue Aug 21 14:27:03 EEST 2012","CreationDate":"2012-08-21T11:33:40Z","Size":387732992,"SHA256TreeHash":"7b58db721ca33bd59af0a43aa8e1346624fa14c0f375629ebd1e304c94ae6916"},{"ArchiveId":"FWE-TlvolJAWlxPWAU8VM_RRCvBbOlAOYsvsTT1hdkCQ3PR9tlO2VnY4a79GuF0amg002cWONZCE0lRnNZSJn61e09SFwN0VfmfoqseMDca29ESAV0JB8RU-bZQgl2cvwJ1TEb6BGQ","ArchiveDescription":"my archive Tue Aug 21 23:40:30 EEST 2012","CreationDate":"2012-08-21T21:03:39Z","Size":133699584,"SHA256TreeHash":"ba0ff1de3f88018930567de38c5698471202da97b3100012fa55067d565c4e8e"},{"ArchiveId":"naGUzlsRxiccy_VkJ-0psc_KCth2IHjjSRvb2_PBAjf6s5iMEKyIdwJLpXnqWQVHLqmKKRFIzkUSn0JBW12bZxZyH9fPGUyneCcAe6Gwz_YX59RMD-wi_t60UHAzvkmEBpwylN50tQ","ArchiveDescription":"my archive Wed Aug 22 11:04:14 EEST 2012","CreationDate":"2012-08-22T08:04:17Z","Size":62145024,"SHA256TreeHash":"6d2f2370c03192a490dd5da297c14cf62a39b8b504f6b5c071bca8bfd4ff7c0e"},{"ArchiveId":"Dv78pEPgKncgz36-MMhbXGp-RBuxaP0_WPPIHHkbHQhG-0oWVJAbe7gjuLVW5OLTLRE2Q3DDfF5AnrrBjaFWhJ6k0uRfrzLt0XL66HpGN4c3qxL1rCF8vfimV2deFlbzZFAX6sZ_2w","ArchiveDescription":"Archived file /tmp/201206 -- Maya Misc.tar Wed Aug 22 11:38:04 EEST 2012","CreationDate":"2012-08-22T08:41:20Z","Size":204649472,"SHA256TreeHash":"27501ed3f241987b3868293f5fae22d3cca9a42ae6870255efb408260f1eebbb"},{"ArchiveId":"n3cYKcg7sng1DNVMFR5QDweAte-_fgaMKZGEWcT2L3vLA_39T__Ql3IgJNsPA7TNAmuR-ZD4npR-zqWcY2xCICkIZlZNpxy6jcKjnOAv0lxpHo8iyVAKcoW5XX3NeShay-BCL2Q3Zg","ArchiveDescription":"Archived file /tmp/201208 -- Borovets.tar Wed Aug 22 11:47:12 EEST 2012","CreationDate":"2012-08-22T08:52:59Z","Size":311491072,"SHA256TreeHash":"523d680f6a1ad78d26d9e86882f7a131a75636eb53a189e6783a13a6f0b87ea7"},{"ArchiveId":"ubKC8A2yZqj9KFHcve_Gf5K-4tL-f3T8YZvHx5qbbVr77yT4L1NARy3OaeP8BGX2qFQ-k6HjC9P7Ry8ZhJLIPiC8OQjgFn1CZjnBa5fJeosqoSfTCsnRH9dGQ9SM8VcKUgVv-2kpQw","ArchiveDescription":"Archived file /tmp/201205 -- Maya Misc.tar Wed Aug 22 11:59:40 EEST 2012","CreationDate":"2012-08-22T09:07:47Z","Size":290558464,"SHA256TreeHash":"fae6fda273bb9650f050fd58d9161f7560bc7280f314599b570a39751d15c2b3"},{"ArchiveId":"cIN4tUkNRw51UNnD3k-MuSAi33zXcLdtNW4K13cTnCT8rrUgvxWmzpTHwS-jRJQPd5Qe5kIRBWfBeMRk6-10cZbASZ70teBgnpsJAK7zEanpe4xfjh8NmqmQiR36EGfgoS_kx7TF_A","ArchiveDescription":"Archived file /tmp/201205 -- Varshets.tar Wed Aug 22 13:47:49 EEST 2012","CreationDate":"2012-08-22T10:54:28Z","Size":317203968,"SHA256TreeHash":"442aa2a47b4bc26309e8832e9b44c8c0c095924d8f8c21ca07f034401ae342ec"},{"ArchiveId":"-HVfZdwdsod2_5DpmN-P6UW6_GweBZs_ot34ypz3ywpkhsPyukeRYxQSYs_ZW464o1xmotHz5Z6q9PD_LPOgRoYsmv_dzkwaArbKAoyOfMt50UplWHmeAiAppemVCnWq203pzwCfbw","ArchiveDescription":"Archived file Home.tar Thu Aug 23 00:45:04 EDT 2012","CreationDate":"2012-08-23T04:43:17Z","Size":69120,"SHA256TreeHash":"010d6110d90c5b749c105fcb3b66b5820d83801f64e00a0729506f85eb7f1097"},{"ArchiveId":"RW-5o0Qodgi7w3WZXcGKZbhTJHvVV0fb0BLbWW5TozEmBQHWW3se88dxijP9c8jP-5nU7-lguywE6Ob0mlsXN0LDShVxVplWWTxBBJaJEWW0SI0EqssOQZ6Y6rvhFybnq-b1CfO1og","ArchiveDescription":"/tmp/Misc-OLD.tar","CreationDate":"2012-08-23T05:05:32Z","Size":15206400,"SHA256TreeHash":"f1ce63c4514b9e8026c2dbb3878c32fab32640406816a9d81bfdb1d2493e1594"}]}. Result handling not yet implemented
				System.out.println(jsonResult + ". Result handling not yet implemented");
			}
			
		}
	}
	*/
	
	private static boolean initializeConfiguration() {
		
		// set variables
		credentials = new BasicAWSCredentials(accessKey, secretKey);
		sdb = new AmazonSimpleDBClient(credentials);
		glacierClient = new AmazonGlacierClient(credentials);
		glacierClient.setEndpoint(endPointList[(endPointInt - 1)]);
		
		// check if sdb credentials are ok?
		if (simpleDbDomainName.length() > 0) {
			try {
				sdb.listDomains();
			} catch (Exception e) {
				System.out.println("Can not access SimpleDB with supplied credentials.");
				return false;
			}
		}
		
		// check if glacier credentials are ok?
		// http://docs.amazonwebservices.com/sdkfornet/latest/apidocs/html/M_Amazon_Glacier_AmazonGlacier_ListJobs.htm
		if (vaultName != null) {
			listInProgressJobsReq = new ListJobsRequest().withStatuscode("InProgress").withVaultName(vaultName);
			try {
				glacierClient.listJobs(listInProgressJobsReq);
			} catch (Exception e) {
				System.out.println("Can not access Glacier service with supplied credentials for vault (" + vaultName + ").");
				return false;
			}
		}
		
		return true;
	}
	
	private static void printWelcome() {
		System.out.println("GlacierFreezer v0.3; simple command line interface to the Amazon Glacier service; http://www.glacierfreezer.com");
	}
	
	private static void printUsage() {
		System.out.println("");
		System.out.println("Usage:");
		System.out.println("");
		System.out.println("To setup the configuration variables run:");
		System.out.println("java -jar glacierFreezer.jar config");
		System.out.println("");
		System.out.println("To upload files to the Glacier, run:");
		System.out.println("java -jar glacierFreezer.jar freeze vaultName /path/to/files/*");
		System.out.println("");
		System.out.println("Options to catalog and view vault contents and retrieve files are coming soon.");
		
	}
	
	private static boolean checkLoadConfiguration() {
		try {
			conf.load(new FileInputStream(propertiesFile));
		} catch (Exception e) {
			return false;
		}
		
		// get all variables
		secretKey = conf.getProperty("secretKey");
		accessKey = conf.getProperty("accessKey");
		simpleDbDomainName = conf.getProperty("simpleDbDomainName");
		endPoint = conf.getProperty("endPoint");
		try { endPointInt = Integer.parseInt(endPoint); } catch (Exception e) { endPoint = null; endPointInt = 0; }
		
		// all properties here?
		if ( (secretKey == null) || (accessKey == null) || (simpleDbDomainName == null) || (endPoint == null) ) {
			return false;
		}
		
		return true;
	}
	
	private static void createConfig() {

		// read input data
    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    	while (true) {
    		System.out.print("Access Key: ");
    		try { accessKey = br.readLine(); } catch (IOException ioe) { System.out.println("IO error trying to read your input!"); System.exit(1); }
    		if (accessKey.length() > 2) { break; }
    		System.out.println("Please enter a valid value!");
    	}
    	while (true) {
    		System.out.print("Secret Key: ");
    		try { secretKey = br.readLine(); } catch (IOException ioe) { System.out.println("IO error trying to read your input!"); System.exit(1); }
    		if (secretKey.length() > 2) { break; }
    		System.out.println("Please enter a valid value!");
    	}
    	while (true) {
    		System.out.print("SimpleDB Domain Name (where information about your archives will be stored. leave empty if you do not wish to use SimpleDb to store this information): ");
    		try { simpleDbDomainName = br.readLine(); } catch (IOException ioe) { System.out.println("IO error trying to read your input!"); System.exit(1); }
    		// dont require this.
    		//if (simpleDbDomainName.length() > 2) { break; }
    		//System.out.println("Please enter a valid value!");
    		break;
    	}
    	System.out.println("Select Your Preferred Region: ");
    	System.out.println("[1]: US East (Northern Virginia) Region");
    	System.out.println("[2]: US West (Oregon) Region");
    	System.out.println("[3]: US West (Northern California) Region");
    	System.out.println("[4]: EU (Ireland) Region");
    	System.out.println("[5]: Asia Pacific (Tokyo) Region");
    	while (true) {
    		System.out.print("Enter a number between 1 and 5: ");
    		try { endPoint = br.readLine(); } catch (IOException ioe) { System.out.println("IO error trying to read your input!"); System.exit(1); }
    		try { endPointInt = Integer.parseInt(endPoint); } catch (Exception e) { endPointInt = 0; }
    		if ((endPointInt > 0) && (endPointInt < 6)) {
    			break;
    		}
    		System.out.println("Please enter a valid value!");
    	}
    	
    	try {
    		conf.setProperty("accessKey", accessKey);
    		conf.setProperty("secretKey", secretKey);
    		conf.setProperty("simpleDbDomainName", simpleDbDomainName);
    		conf.setProperty("endPoint", endPoint);
    		
    		// save properties
    		conf.store(new FileOutputStream(propertiesFile), null);
 
    	} catch (IOException e) {
    		//e.printStackTrace();
    		//return false;
    		System.out.println("Unable to save configuration file to " + propertiesFile + ". Please check your permissions!"); System.exit(1);
        }
    	
    	System.out.println("Configuration saved to " + propertiesFile);
    	System.exit(0);
    	
	}
	
	public static void main(String[] args) {
		
		printWelcome();
		
		// check if configuration is to be run?
		if (((args.length > 0) && (args[0].toLowerCase().equals("config"))) || (!checkLoadConfiguration())) {
			System.out.println("Creating new configuration file.");
			createConfig();
			System.exit(0);
		}
		
		// upload?
		if ((args.length > 2) && (args[0].toLowerCase().equals("freeze"))) {
			
			vaultName = args[1];
			int frozenCount = 0;
			
			// validate configuration
			if (!initializeConfiguration()) {
				System.out.println("There was a problem with your current configuration. Please check the error messages above and run the config option to setup a new configuration.");
				System.exit(1);
			}
			
			/*
			this code here looks up files with the patten matcher; this requires for the patterns to be speicifed
			in quotes on the command line. for now disable this, all matched files will be passed through the args by
			the OS.

			String filePathNamePattern = args[2];
			String filePathOnly = new File(filePathNamePattern).getParent();
			String fileNamePatternOnly = "";
			
			if (filePathOnly == null) { 
				filePathOnly = "."; 
				fileNamePatternOnly = filePathNamePattern;
			} else {
				fileNamePatternOnly = new File(filePathNamePattern).getName();
			}
			File dir = new File(filePathOnly + "/");
			FileFilter fileFilter = new WildcardFileFilter(fileNamePatternOnly);
			File[] files = dir.listFiles(fileFilter);
			for (int i = 0; i < files.length; i++) {
				freezeFile(files[i].getAbsolutePath());
				frozenCount++;
			}
			*/
			
			for (int i = 2; i < args.length; i++) {
				//System.out.println(args[i]);
				freezeFile(args[i]);
				frozenCount++;
			}
			
			if (frozenCount < 1) {
				System.out.println("No files matching the specified pattern were found.");
			}
			
			System.exit(0);
		}
		
		// TODO vault inventory
		/*
		if ((args.length > 1) && (args[0].toLowerCase().equals("reconcile"))) {
		
			vaultName = args[1];
			
			// validate configuration
			if (!initializeConfiguration()) {
				System.out.println("There was a problem with your current configuration. Please check the error messages above and run the config option to setup a new configuration.");
				System.exit(1);
			}
			
			reconcileVault();
			System.exit(0);
		}
		*/

		printUsage();
		
	}

}
